#include <iostream>
#include <stdio.h>
#include <string.h>
#include <image.hpp>

int main()
{

    ImageLoad imagen("../imgs/image.png");

    unsigned int b = 31;

    unsigned int *hist_binning = get_hist(imagen, 'r', b);

    for (std::size_t i{}; i < b; i++)
    {
        printf("%lu: %u\n", i, hist_binning[i]);
    }

    return 0;
}
